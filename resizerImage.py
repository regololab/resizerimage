#!/usr/bin/env python
#-*- coding:utf-8 -*-

from PIL import Image
import sys
import os

class ResizeImgDir(object):
    """ Class doc """

    def __init__ (self):
        """ Class initialiser """
        self.Cartella = raw_input("Specificare percorso directory \n>>> ")
        if self.Cartella == 'i' or self.Cartella == 'I':
            print "+-----------------------------------------------------------------------------------+"
            print "| ResizeImgDir v b0.1:                                                              |"
            print "| Realizzaro da Leo Lo Tito 2014                                                    |"
            print "| Rif.: leo.lotito@gmail.com                                                        |"
            print "| Potete copiare, regalare, distribuire... fare quello che volete di questo codice  |"
            print "| Ovvimente non mi assumo alcuna responsabilità per danni a cose o persone :-D      |"
            print "| Un GRAZIE di CUORE a chiunche utilizzando lo script lasci un mio riferimento      |"
            print "+-----------------------------------------------------------------------------------+\n"
            continua = raw_input('Vuoi Continuare? S = Si; N = No:\n>>')
            if continua == 'S' or continua == 's':
                self.Cartella = raw_input("Specificare percorso directory \n>>> ")
            else:
                sys.exit()
        #self.Cartella = '/home/leo/Scrivania/modI/'
        width = raw_input("Specificare Larghezza massima (in PX) \n>>> ")
        height = raw_input("Specificare Altezza massima (in PX) \n>>> ")
        self.dpi = raw_input("Specificare il DPI (in PX) \n>>> ")
        self.dpi = int(self.dpi)
        self.imageMax = int(width), int(height)
        self.extension = ['.JPG', '.jpg', '.Jpeg', '.JPEG', '.jpeg', '.gif', '.GIF', '.png', '.PNG']

    def resizeImg(self):
        listDir = os.listdir(self.Cartella)
        W = 0
        H = 0
        for Rimage in listDir:
            fn, ext = os.path.splitext(self.Cartella+Rimage)
            if ext in self.extension:
                img = Image.open(fn+ext)
                # se esiste il DPI
                try:
                    dpix = img.info["dpi"]
                except:
                    dpix = (72, 72)

                W = img.size[0]
                H = img.size[1]
                # provvede a ridimensionare le immagini
                if W > self.imageMax[0]:
                    im = Image.open(self.Cartella+Rimage)
                    im.thumbnail(self.imageMax, Image.ANTIALIAS)
                    if ext == '.JPG' or ext == '.jpg' or ext == '.Jpeg' or ext == '.JPEG' or ext == '.jpeg':
                        im.save(fn+ext.replace(" ", ""), "JPEG", dpi=(self.dpi,self.dpi))
                    elif ext == '.gif' or ext == '.GIF':
                        im.save(fn+ext.replace(" ", ""), "GIF", dpi=(self.dpi,self.dpi))
                    elif ext == '.png' or ext == '.PNG':
                        im.save(fn+ext.replace(" ", ""), "PNG", dpi=(self.dpi,self.dpi))
                elif H > self.imageMax[1]:
                    im = Image.open(self.Cartella+Rimage)
                    im.thumbnail(self.imageMax, Image.ANTIALIAS)
                    if ext == '.JPG' or ext == '.jpg' or ext == '.Jpeg' or ext == '.JPEG' or ext == '.jpeg':
                        im.save(fn+ext.replace(" ", ""), "JPEG", dpi=(self.dpi,self.dpi))
                    elif ext == '.gif' or ext == '.GIF':
                        im.save(fn+ext.replace(" ", ""), "GIF", dpi=(self.dpi,self.dpi))
                    elif ext == '.png' or ext == '.PNG':
                        im.save(fn+ext.replace(" ", ""), "PNG", dpi=(self.dpi,self.dpi))

                # se l'immagine è più piccola della richiesta in PX ma più grande in DPI procede a riconfigurarla
                if dpix[0] > self.dpi or dpix[1] > self.dpi:
                    im = Image.open(self.Cartella+Rimage)
                    if ext == '.JPG' or ext == '.jpg' or ext == '.Jpeg' or ext == '.JPEG' or ext == '.jpeg':
                        im.save(fn+ext.replace(" ", ""), "JPEG", dpi=(self.dpi,self.dpi))
                    elif ext == '.gif' or ext == '.GIF':
                        im.save(fn+ext.replace(" ", ""), "GIF", dpi=(self.dpi,self.dpi))
                    elif ext == '.png' or ext == '.PNG':
                        im.save(fn+ext.replace(" ", ""), "PNG", dpi=(self.dpi,self.dpi))

                print fn+ext


ResizeDir = ResizeImgDir()
ResizeDir.resizeImg()