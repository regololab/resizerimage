# README #

Come utilizzare resizerImage:

installare la libreria PIL di python (per debian e derivate)

* sudo apt-get install libjpeg-dev
* sudo apt-get install python-pip
* pip install PILLOW

Lanciare il software:

**python resizerImage.py**

seguire le istruzioni (ad esempio):

**Specificare percorso directory**

*/home/mypc/Scrivania/immagini/*

**Specificare Larghezza massima (in PX)**

*400*

**Specificare Altezza massima (in PX)**

*270*

**Specificare il DPI (in PX)** 

*72*

[....]

**Attenzione**:

* le immagini della directory saranno ridimensionate, quindi è opportuno fare una copia delle immagini se non si desidera perdere le originali.

* Le immagini saranno ridimensionate proporzionalmente, le misure inserite saranno approssimative